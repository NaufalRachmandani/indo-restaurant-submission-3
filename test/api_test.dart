import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail_result.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant_result.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'api_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  group('Restaurant API Check', () {
    test('Return List Restaurant', () async {
      final client = MockClient();

      when(client.get(Uri.parse("https://restaurant-api.dicoding.dev/list")))
          .thenAnswer((_) async => http.Response(
              '{"error":false,"message":"success","count":20,"restaurants":[]}',
              200));

      expect(await ApiService(client).getRestaurantList(),
          isA<RestaurantListResult>());
    });

    test('Return Detail Restaurant Kafein', () async {
      final client = MockClient();

      when(client.get(Uri.parse(
              "https://restaurant-api.dicoding.dev/detail/uewq1zg2zlskfw1e867")))
          .thenAnswer((_) async => http.Response('''
          {
    "error": false,
    "message": "success",
    "restaurant": {
        "id": "uewq1zg2zlskfw1e867",
        "name": "Kafein",
        "description": "Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,",
        "city": "Aceh",
        "address": "Jln. Belimbing Timur no 27",
        "pictureId": "15",
        "rating": 4.6,
        "categories": [
            {
                "name": "Italia"
            },
            {
                "name": "Jawa"
            }
        ],
        "menus": {
            "foods": [
                {
                    "name": "Salad lengkeng"
                },
                {
                    "name": "Kari terong"
                },
                {
                    "name": "Sosis squash dan mint"
                },
                {
                    "name": "Napolitana"
                },
                {
                    "name": "Salad yuzu"
                },
                {
                    "name": "Tumis leek"
                },
                {
                    "name": "Ikan teri dan roti"
                },
                {
                    "name": "Kari kacang dan telur"
                },
                {
                    "name": "Toastie salmon"
                },
                {
                    "name": "Bebek crepes"
                },
                {
                    "name": "Paket rosemary"
                },
                {
                    "name": "Sup Kohlrabi"
                },
                {
                    "name": "roket penne"
                }
            ],
            "drinks": [
                {
                    "name": "Minuman soda"
                },
                {
                    "name": "Es teh"
                },
                {
                    "name": "Jus tomat"
                },
                {
                    "name": "Coklat panas"
                },
                {
                    "name": "Jus jeruk"
                },
                {
                    "name": "Air"
                },
                {
                    "name": "Kopi espresso"
                },
                {
                    "name": "Es kopi"
                },
                {
                    "name": "Sirup"
                },
                {
                    "name": "Jus alpukat"
                },
                {
                    "name": "Jus mangga"
                },
                {
                    "name": "Jus apel"
                },
                {
                    "name": "Es krim"
                }
            ]
        },
        "customerReviews": [
            {
                "name": "Gilang",
                "review": "Saya sangat suka menu malamnya!",
                "date": "13 November 2019"
            },
            {
                "name": "Ahmad",
                "review": "Tempatnya bagus namun menurut saya masih sedikit mahal.",
                "date": "14 Agustus 2018"
            }
        ]
    }
}
''', 200));
      expect(await ApiService(client).getRestaurant("uewq1zg2zlskfw1e867"),
          isA<RestaurantDetailResult>());
    });
  });
}
