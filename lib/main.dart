import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/menu_list.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/customer_review_list.dart';
import 'package:indo_restaurant/provider/restaurant_provider.dart';
import 'package:indo_restaurant/provider/scheduling_provider.dart';
import 'package:indo_restaurant/service/background_service.dart';
import 'package:indo_restaurant/ui/bookmark/bookmarked_list_page.dart';
import 'package:indo_restaurant/ui/detail/detail_restaurant_page.dart';
import 'package:indo_restaurant/ui/home/restaurant_list_page.dart';
import 'package:indo_restaurant/ui/search/search_page.dart';
import 'package:indo_restaurant/ui/settings/setting_page.dart';
import 'package:indo_restaurant/ui/splash_screen.dart';
import 'package:indo_restaurant/utils/notif_helper.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:provider/provider.dart';

import 'common/navigators.dart';
import 'common/style.dart';
import 'data/api/api_service.dart';
import 'data/model/RestaurantDetail/category.dart';
import 'data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'data/model/RestaurantDetail/restaurant_detail.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  var appDocumentDirectory = await path.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(RestaurantDetailAdapter());
  Hive.registerAdapter(CategoryAdapter());
  Hive.registerAdapter(CustomerReviewListAdapter());
  Hive.registerAdapter(MenuListAdapter());
  await Hive.openBox<RestaurantDetail>("Restaurant");

  final BackgroundService _service = BackgroundService();
  _service.initializeIsolate();
  await AndroidAlarmManager.initialize();

  final NotifHelper _notificationHelper = NotifHelper();
  await _notificationHelper.initializeNotif(flutterLocalNotificationsPlugin);
  _notificationHelper
      .configSelectNotifSubject(DetailRestaurantPage.routeName);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final ThemeData theme = ThemeData();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => RestaurantProvider(apiService: ApiService(Client())),
        ),
        ChangeNotifierProvider(
          create: (_) => SchedulingProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Indo Restaurant',
        navigatorKey: navKey,
        theme: theme.copyWith(
          colorScheme: theme.colorScheme.copyWith(
            primary: primary,
            secondary: secondary,
          ),
        ),
        initialRoute: SplashScreen.routeName,
        routes: {
          SplashScreen.routeName: (context) => SplashScreen(),
          RestaurantListPage.routeName: (context) => RestaurantListPage(),
          SearchPage.routeName: (context) => SearchPage(),
          DetailRestaurantPage.routeName: (context) => DetailRestaurantPage(
              ModalRoute.of(context)?.settings.arguments
                  as DetailRestaurantArgs),
          BookmarkedListPage.routeName: (context) => BookmarkedListPage(),
          SettingPage.routeName: (context) => SettingPage(),
        },
      ),
    );
  }
}
