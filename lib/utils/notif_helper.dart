import 'dart:convert';
import 'dart:math';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:indo_restaurant/common/navigators.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant_result.dart';
import 'package:rxdart/rxdart.dart';

class NotifHelper {
  final notificationSubject = BehaviorSubject<String>();
  var randomNumber = 1 + Random().nextInt(10 - 1);

  static NotifHelper? _instance;

  NotifHelper._internal() {
    _instance = this;
  }

  factory NotifHelper() => _instance ?? NotifHelper._internal();

  Future<void> initializeNotif(
      FlutterLocalNotificationsPlugin localNotif) async {
    await localNotif.initialize(
        InitializationSettings(android: AndroidInitializationSettings('logo')),
        onSelectNotification: (String? payload) async {
      notificationSubject.add(payload ?? 'empty payload');
    });
  }

  Future<void> showNotif(FlutterLocalNotificationsPlugin localNotif,
      RestaurantListResult restaurantListResult) async {
    var _title = restaurantListResult.restaurantList[randomNumber].name;
    var _location = restaurantListResult.restaurantList[randomNumber].city;

    var androidNotifChannel = NotificationDetails(
        android: AndroidNotificationDetails("01", "channel01", "Rstaurant Notification",
            importance: Importance.max,
            priority: Priority.high,
            ticker: '',
            styleInformation: BigPictureStyleInformation(
              DrawableResourceAndroidBitmap("logo"),
              largeIcon: DrawableResourceAndroidBitmap("logo"),
              contentTitle: _title,
              summaryText: _location,
            )));

    await localNotif.show(0, _title, _location, androidNotifChannel,
        payload: json.encode(restaurantListResult.toJson()));
  }

  void configSelectNotifSubject(String route) {
    notificationSubject.stream.listen(
      (String payload) async {
        var data = RestaurantListResult.fromJson(json.decode(payload));
        var restaurant = data.restaurantList[randomNumber];
        Navigators.navigateData(
            route, DetailRestaurantArgs(restaurant.id, restaurant.name));
      },
    );
  }
}
