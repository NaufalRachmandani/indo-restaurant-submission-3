import 'package:intl/intl.dart';

class DateHelper {
  static DateTime format() {
    final now = DateTime.now();
    final dateFormat = DateFormat('y/M/d');
    final time = "11:00:00";
    final completeFormat = DateFormat('y/M/d H:m:s');

    // if now < time
    final dateToday = dateFormat.format(now);
    final todayDateAndTime = "$dateToday $time";
    var resultToday = completeFormat.parseStrict(todayDateAndTime);

    // if now > time
    var addDuration = resultToday.add(Duration(days: 1));
    final dateTomorrow = dateFormat.format(addDuration);
    final tomorrowDateAndTime = "$dateTomorrow $time";
    var resultTomorrow = completeFormat.parseStrict(tomorrowDateAndTime);

    return now.isAfter(resultToday) ? resultTomorrow : resultToday;
  }
}
