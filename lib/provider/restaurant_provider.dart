import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail_result.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/add_review_result.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/customer_review_list.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant_result.dart';
import 'package:indo_restaurant/utils/status.dart';

class RestaurantProvider extends ChangeNotifier {
  final ApiService apiService;

  RestaurantProvider({required this.apiService}) {
    getRestaurants();
    getSearchRestaurants("a");
    getBox();
  }

  RestaurantListResult? _restaurantResult;
  RestaurantDetailResult? _restaurantDetailResult;
  AddReviewResult? _addReviewResult;

  String _message = '';

  String get message => _message;
  Status _state = Status.Loading;

  Status get state => _state;

  String _messageSearch = '';

  String get messageSearch => _messageSearch;
  Status _stateSearch = Status.Loading;

  Status get stateSearch => _stateSearch;

  String _messageDetail = '';

  String get messageDetail => _messageDetail;
  Status _stateDetail = Status.Loading;

  Status get stateDetail => _stateDetail;

  String _messageReview = '';

  String get messageReview => _messageReview;
  Status _stateReview = Status.NoData;

  Status get stateReview => _stateReview;

  RestaurantDetailResult? get restaurantDetailResult => _restaurantDetailResult;

  List<Restaurant> get restaurantList =>
      _restaurantResult?.restaurantList ?? [];

  List<Restaurant> _searchedRestaurantList = [];
  List<Restaurant> get searchedRestaurantList => _searchedRestaurantList;

  List<CustomerReviewList> get reviewList {
    if (_addReviewResult?.customerReviewList.isNotEmpty == true) {
      return _addReviewResult?.customerReviewList ?? [];
    } else {
      return _restaurantDetailResult?.restaurantDetail.customerReviewList ?? [];
    }
  }

  late Box _box;

  Box get box => _box;
  Status _stateBookmark = Status.NoData;

  Status get stateBookmark => _stateBookmark;
  List<RestaurantDetail> _bookmarkedList = [];

  List<RestaurantDetail> get bookmarkedList => _bookmarkedList;

  Future<Box> getBox() async {
    var box = await Hive.openBox<RestaurantDetail>("Restaurant");
    return _box = box;
  }

  Future<dynamic> getBookmarkedRestaurantDetail() async {
    final restaurants = box.values.toList().cast<RestaurantDetail>();
    if (restaurants.isEmpty) {
      _stateBookmark = Status.NoData;
      notifyListeners();
      return _message = 'Data restaurant yang telah dibookmark tidak ditemukan';
    } else {
      _stateBookmark = Status.HasData;
      _bookmarkedList = restaurants;
      notifyListeners();
    }
  }

  Future<dynamic> addRestaurantDetailToBookmarked(
      RestaurantDetail restaurant) async {
    box.add(restaurant);
    notifyListeners();
  }

  Future<dynamic> deleteRestaurantDetailFromBookmarked(
      RestaurantDetail restaurant) async {
    box.delete(restaurant.key);
    notifyListeners();
  }

  Future<dynamic> deleteBookmarkedList() async {
    box.clear();
    _bookmarkedList.clear();
    _stateBookmark = Status.NoData;
    notifyListeners();
  }

  bool isRestaurantDetailBookmarked(RestaurantDetail restaurant) {
    final restaurants = box.values.toList().cast<RestaurantDetail>();
    print("$restaurants");
    var temp = restaurants
        .where((element) => element.name == restaurant.name)
        .isNotEmpty;
    return temp;
  }

  Future<dynamic> getRestaurants() async {
    try {
      _state = Status.Loading;
      notifyListeners();
      final restaurants = await apiService.getRestaurantList();
      if (restaurants.restaurantList.isEmpty) {
        _state = Status.NoData;
        notifyListeners();
        return _message = 'Restaurant tidak ditemukan';
      } else {
        _state = Status.HasData;
        notifyListeners();
        return _restaurantResult = restaurants;
      }
    } catch (e) {
      _state = Status.Error;
      notifyListeners();
      print('Error --> $e');
      return _message = 'Maaf, gagal mendapatkan data dari internet';
    }
  }

  Future<dynamic> getRestaurantDetail(String id) async {
    try {
      _stateDetail = Status.Loading;
      final restaurant = await apiService.getRestaurant(id);
      if (restaurant.restaurantDetail.id.isEmpty) {
        _stateDetail = Status.NoData;
        notifyListeners();
        return _message = 'Restaurant tidak ditemukan';
      } else {
        _stateDetail = Status.HasData;
        notifyListeners();
        print(
            'reviews : ${restaurant.restaurantDetail.customerReviewList.length}');
        _stateReview = Status.HasData;
        return _restaurantDetailResult = restaurant;
      }
    } catch (e) {
      _stateDetail = Status.Error;
      notifyListeners();
      print('Error --> $e');
      return _messageDetail = 'Maaf, gagal mendapatkan data dari internet';
    }
  }

  Future<dynamic> addRestaurantReview(
      String id, String name, String review) async {
    try {
      _stateReview = Status.Loading;
      notifyListeners();
      final response = await apiService.addReview(id, name, review);
      _stateReview = Status.HasData;
      notifyListeners();
      return _addReviewResult = response;
    } catch (e) {
      _stateReview = Status.Error;
      notifyListeners();
      print('Error --> $e');
      return _messageReview = 'Maaf, gagal menambahkan review';
    }
  }

  Future<dynamic> getSearchRestaurants(String query) async {
    _searchedRestaurantList.clear();
    try {
      _stateSearch = Status.Loading;
      notifyListeners();
      final restaurants = await apiService.searchRestaurantList(query);
      if (restaurants.restaurantList.isEmpty) {
        _stateSearch = Status.NoData;
        notifyListeners();
        return _messageSearch = 'Restaurant tidak ditemukan';
      } else {
        _stateSearch = Status.HasData;
        notifyListeners();
        return _searchedRestaurantList = restaurants.restaurantList;
      }
    } catch (e) {
      _stateSearch = Status.Error;
      notifyListeners();
      print('Error --> $e');
      return _messageSearch = 'Maaf, gagal mendapatkan data dari internet';
    }
  }
}
