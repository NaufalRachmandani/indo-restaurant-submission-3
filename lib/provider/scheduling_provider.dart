import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:indo_restaurant/service/background_service.dart';
import 'package:indo_restaurant/utils/date_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SchedulingProvider extends ChangeNotifier {
  bool _isScheduled = false;
  bool get isScheduled => _isScheduled;

  final _scheduleId = 1;

  Future<bool> scheduledRestaurant(bool scheduled) async {
    _isScheduled = scheduled;
    if (_isScheduled) {
      print('Scheduling activated');
      notifyListeners();
      return await AndroidAlarmManager.periodic(
        Duration(hours: 24),
        _scheduleId,
        BackgroundService.callback,
        startAt: DateHelper.format(),
        exact: true,
        wakeup: true,
      );
    } else {
      print('Scheduling canceled');
      notifyListeners();
      return await AndroidAlarmManager.cancel(_scheduleId);
    }
  }

  void saveSwitchState(bool val) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("switch", val);
  }

  void getSwitchState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool val = prefs.getBool("switch") ?? false;
    _isScheduled = val;
    notifyListeners();
  }
}
