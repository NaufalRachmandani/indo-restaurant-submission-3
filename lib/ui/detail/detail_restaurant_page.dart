import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:indo_restaurant/common/style.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/customer_review_list.dart';
import 'package:indo_restaurant/provider/restaurant_provider.dart';
import 'package:indo_restaurant/utils/status.dart';
import 'package:provider/provider.dart';

class DetailRestaurantPage extends StatefulWidget {
  static const routeName = '/detail_restaurant_page';

  final DetailRestaurantArgs _detailRestaurantArguments;
  late final String _restaurantId;
  late final String _restaurantName;

  DetailRestaurantPage(this._detailRestaurantArguments) {
    _restaurantId = _detailRestaurantArguments.restaurantId;
    _restaurantName = _detailRestaurantArguments.restaurantName;
  }

  @override
  _DetailRestaurantPageState createState() => _DetailRestaurantPageState();
}

class _DetailRestaurantPageState extends State<DetailRestaurantPage> {
  final TextEditingController _nameController = TextEditingController();
  String _name = "";

  final TextEditingController _reviewController = TextEditingController();
  String _review = "";

  @override
  void initState() {
    Provider.of<RestaurantProvider>(context, listen: false)
        .getRestaurantDetail(widget._restaurantId);

    _nameController.addListener(
          () {
        setState(() {
          _name = _nameController.text;
        });
      },
    );

    _reviewController.addListener(
          () {
        setState(() {
          _review = _reviewController.text;
        });
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._restaurantName),
      ),
      body: Consumer<RestaurantProvider>(
        builder: (context, state, _) {
          if (state.stateDetail == Status.Loading) {
            return Center(child: CircularProgressIndicator());
          } else if (state.stateDetail == Status.NoData) {
            return Center(child: Text(state.messageDetail));
          } else if (state.stateDetail == Status.Error) {
            return Center(
              child: Text(state.messageDetail),
            );
          } else if (state.stateDetail == Status.HasData) {
            if (state.restaurantDetailResult != null) {
              return restaurantDetailContainer(
                  context, state.restaurantDetailResult!.restaurantDetail);
            } else {
              return Center(child: Text('gagal memuat data dari internet'));
            }
          } else {
            return Center(child: Text('gagal memuat data dari internet'));
          }
        },
      ),
    );
  }

  Widget restaurantDetailContainer(BuildContext context,
      RestaurantDetail restaurantDetail) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 40),
        child:
        Container(child: buildrRestaurantDetail(context, restaurantDetail)),
      ),
    );
  }

  Widget buildrRestaurantDetail(BuildContext context,
      RestaurantDetail restaurantDetail) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 2 / 5,
                height: 200,
                padding: EdgeInsets.only(left: 10, top: 10),
                child: restaurantImage(restaurantDetail)),
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width * 3 / 5 - 20,
              height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  restaurantName(restaurantDetail),
                  restaurantCity(restaurantDetail),
                  restaurantRating(restaurantDetail),
                ],
              ),
            ),
          ],
        ),
        restaurantDescription(restaurantDetail),
        foodsSection(restaurantDetail),
        drinksSection(restaurantDetail),
        reviewSection(restaurantDetail),
        addReviewSection(restaurantDetail),
      ],
    );
  }

  Widget restaurantImage(RestaurantDetail restaurantDetail) {
    return Consumer<RestaurantProvider>(
        builder: (context, state, _) {
          return Stack(
            children: [
              Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 2 / 5,
                height: 200,
                child: Hero(
                  tag: restaurantDetail.pictureId,
                  child: Image.network(
                    ApiService.BASE_PIC + restaurantDetail.pictureId,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Positioned(
                bottom: 10,
                right: 10,
                child: (state.isRestaurantDetailBookmarked(restaurantDetail)) ? ElevatedButton(
                  onPressed: () {
                    state.deleteRestaurantDetailFromBookmarked(restaurantDetail);
                  },
                  child: Icon(
                    Icons.bookmark,
                    color: Colors.white,
                  ),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(CircleBorder()),
                    padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                    backgroundColor: MaterialStateProperty.all(primary),
                    // <-- Button color
                    overlayColor:
                    MaterialStateProperty.resolveWith<Color?>((states) {
                      if (states.contains(MaterialState.pressed))
                        return secondary;
                    }),
                  ),
                ) : ElevatedButton(
                  onPressed: () {
                    state.addRestaurantDetailToBookmarked(restaurantDetail);
                  },
                  child: Icon(
                    Icons.bookmark_border,
                    color: Colors.white,
                  ),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(CircleBorder()),
                    padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                    backgroundColor: MaterialStateProperty.all(Colors.grey),
                    // <-- Button color
                    overlayColor:
                    MaterialStateProperty.resolveWith<Color?>((states) {
                      if (states.contains(MaterialState.pressed))
                        return secondary;
                    }),
                  ),
                ),
              ),
            ]
          );
        }
    );
  }

  Widget restaurantName(RestaurantDetail restaurantDetail) {
    return Container(
      padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
      child: Text(
        restaurantDetail.name,
        style: blackTextTheme.headline4,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget restaurantCity(RestaurantDetail restaurantDetail) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Icon(
            Icons.location_on,
            size: 30,
            color: secondary,
          ),
          Text(
            restaurantDetail.city,
            style: blackTextTheme.headline6,
          ),
        ],
      ),
    );
  }

  Widget restaurantRating(RestaurantDetail restaurantDetail) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          RatingBar.builder(
            initialRating: restaurantDetail.rating.toDouble(),
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemSize: 30,
            itemBuilder: (context, _) =>
                Icon(
                  Icons.star,
                  color: Colors.yellow,
                ),
            onRatingUpdate: (rating) {},
          ),
        ],
      ),
    );
  }

  Widget restaurantDescription(RestaurantDetail restaurantDetail) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [
          Text(
            restaurantDetail.description,
            style: blackTextTheme.subtitle1,
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  Widget foodsSection(RestaurantDetail restaurantDetail) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Row(
              children: [
                Image.asset(
                  'images/burger.png',
                  height: 40,
                  width: 40,
                ),
                Text(
                  'Makan',
                  style: blackTextTheme.headline6,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 50,
            child: ListView.builder(
              padding: EdgeInsets.only(left: 10),
              scrollDirection: Axis.horizontal,
              itemCount: restaurantDetail.menuList.foodList.length,
              itemBuilder: (context, index) {
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        restaurantDetail.menuList.foodList
                            .elementAt(index)
                            .name,
                        style: blackTextTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget drinksSection(RestaurantDetail restaurantDetail) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Row(
              children: [
                Image.asset(
                  'images/drink.png',
                  height: 40,
                  width: 40,
                ),
                Text(
                  'Minum',
                  style: blackTextTheme.headline6,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 50,
            child: ListView.builder(
              padding: EdgeInsets.only(left: 10),
              scrollDirection: Axis.horizontal,
              itemCount: restaurantDetail.menuList.drinkList.length,
              itemBuilder: (context, index) {
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        restaurantDetail.menuList.drinkList
                            .elementAt(index)
                            .name,
                        style: blackTextTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget reviewSection(RestaurantDetail restaurantDetail) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 20, left: 10),
            child: Text(
              'Review',
              style: blackTextTheme.headline6,
            ),
          ),
          Container(
            child: Consumer<RestaurantProvider>(
              builder: (context, state, _) {
                if (state.stateReview == Status.Loading) {
                  return Center(child: CircularProgressIndicator());
                } else if (state.stateReview == Status.HasData) {
                  return listViewAdapter(state.reviewList);
                } else if (state.stateReview == Status.Error) {
                  return Center(
                    child: Text(state.messageReview),
                  );
                } else if (state.stateReview == Status.NoData) {
                  return Center(child: Text('tidak ada data'));
                } else {
                  return Center(child: Text('gagal memuat data'));
                }
              },
            ),
          )
        ],
      ),
    );
  }

  Widget listViewAdapter(List<CustomerReviewList> reviews) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: reviews.length,
      itemBuilder: (context, index) {
        return Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  reviews[index].name,
                  style: blackTextTheme.bodyText1,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Text(
                  reviews[index].review,
                  style: blackTextTheme.subtitle1,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget addReviewSection(RestaurantDetail restaurantDetail) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textFieldReviewSection(),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(top: 10),
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: ElevatedButton(
                  child: Text(
                    'Tambah Review',
                    style: whiteTextTheme.button,
                  ),
                  style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all<Color>(primary),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ))),
                  onPressed: () {
                    if (_review.isEmpty || _name.isEmpty) {
                      print("Harap isi review dengan benar");
                    } else {
                      Provider.of<RestaurantProvider>(context, listen: false)
                          .addRestaurantReview(
                          widget._restaurantId, _name, _review);
                    }
                  },
                ),
              )),
        ],
      ),
    );
  }

  Widget textFieldReviewSection() {
    return Container(
      child: Column(
        children: [
          nameField(),
          reviewField(),
        ],
      ),
    );
  }

  Widget nameField() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: primary, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: TextField(
        decoration: InputDecoration(
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          hintText: "Masukkan nama kamu",
          suffixIcon: _name.isEmpty
              ? null
              : IconButton(
            onPressed: () {
              _nameController.clear();
            },
            icon: Icon(Icons.cancel, color: Colors.grey),
          ),
        ),
        controller: _nameController,
        autofocus: false,
        style: blackTextTheme.subtitle1,
        textCapitalization: TextCapitalization.words,
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget reviewField() {
    return Container(
        margin: EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          border: Border.all(color: primary, width: 1),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: TextField(
          decoration: InputDecoration(
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            hintText: "Tulis review kamu dengan jujur",
            suffixIcon: _review.isEmpty
                ? null
                : IconButton(
              onPressed: () {
                _reviewController.clear();
              },
              icon: Icon(Icons.cancel, color: Colors.grey),
            ),
          ),
          textInputAction: TextInputAction.newline,
          keyboardType: TextInputType.multiline,
          minLines: 1,
          maxLines: 10,
          controller: _reviewController,
          autofocus: false,
          style: blackTextTheme.subtitle1,
          textCapitalization: TextCapitalization.words,
        ));
  }
}
