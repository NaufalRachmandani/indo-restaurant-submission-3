import 'package:flutter/material.dart';
import 'package:indo_restaurant/common/navigators.dart';
import 'package:indo_restaurant/common/style.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant.dart';
import 'package:indo_restaurant/provider/restaurant_provider.dart';
import 'package:indo_restaurant/ui/bookmark/bookmarked_list_page.dart';
import 'package:indo_restaurant/ui/detail/detail_restaurant_page.dart';
import 'package:indo_restaurant/ui/search/search_page.dart';
import 'package:indo_restaurant/ui/settings/setting_page.dart';
import 'package:indo_restaurant/utils/status.dart';
import 'package:provider/provider.dart';

class RestaurantListPage extends StatefulWidget {
  static const routeName = '/restaurant_list_page';

  @override
  _RestaurantListPageState createState() => _RestaurantListPageState();
}

class _RestaurantListPageState extends State<RestaurantListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NestedScrollView(
          headerSliverBuilder: (context, isScrolled) {
            return [
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image.asset(
                    'images/banner.png',
                    fit: BoxFit.fitWidth,
                  ),
                  title: Text('Indo Restaurant'),
                  titlePadding: const EdgeInsets.only(left: 16, bottom: 16),
                ),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigators.navigate(SearchPage.routeName);
                        },
                        child: Icon(Icons.search),
                      )),
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {
                          Provider.of<RestaurantProvider>(context,
                                  listen: false)
                              .getBookmarkedRestaurantDetail();
                          Navigators.navigate(BookmarkedListPage.routeName);
                        },
                        child: Icon(Icons.bookmark),
                      )),
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigators.navigate(SettingPage.routeName);
                        },
                        child: Icon(Icons.settings),
                      )),
                ],
              ),
            ];
          },
          body: buildList(context)),
    );
  }

  Widget buildList(BuildContext context) {
    return Consumer<RestaurantProvider>(
      builder: (context, state, _) {
        if (state.state == Status.Loading) {
          return Center(child: CircularProgressIndicator());
        } else if (state.state == Status.HasData) {
          return listViewAdapter(state.restaurantList);
        } else if (state.state == Status.NoData) {
          return Center(child: Text(state.message));
        } else if (state.state == Status.Error) {
          return Center(
            child: Text('gagal memuat data : ' + state.message),
          );
        } else {
          return Center(child: Text('gagal memuat data'));
        }
      },
    );
  }

  Widget listViewAdapter(List<Restaurant> restaurants) {
    return ListView.builder(
        padding: EdgeInsets.only(top: 10),
        shrinkWrap: true,
        itemCount: restaurants.length,
        itemBuilder: (context, index) =>
            Consumer<RestaurantProvider>(builder: (context, state, child) {
              return itemRestaurant(state.restaurantList[index]);
            }));
  }

  Widget itemRestaurant(Restaurant restaurant) {
    return GestureDetector(
      onTap: () {
        Navigators.navigateData(DetailRestaurantPage.routeName,
            DetailRestaurantArgs(restaurant.id, restaurant.name));
      },
      child: Container(
        child: Card(
          color: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
          child: Row(
            children: [
              pictureAndRatingSection(restaurant.pictureId, restaurant.rating),
              nameSection(restaurant.name),
            ],
          ),
        ),
      ),
    );
  }

  Widget nameSection(String restaurantName) {
    return Container(
      width: MediaQuery.of(context).size.width * 5 / 6 - 25,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          name(restaurantName),
        ],
      ),
    );
  }

  Widget pictureAndRatingSection(String pictureId, double rating) {
    return Container(
        margin: EdgeInsets.only(left: 5, bottom: 5, top: 5),
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.width * 1 / 6 + 12,
              color: Colors.white,
            ),
            restaurantPicture(pictureId),
            Positioned(
                bottom: 0,
                left: 2,
                right: 2,
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: primary,
                          spreadRadius: 1,
                          blurRadius: 1,
                        ),
                      ],
                    ),
                    child: ratingSection(rating))),
          ],
        ));
  }

  Widget restaurantPicture(String pictureId) {
    return Hero(
      tag: pictureId,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.network(
          ApiService.BASE_PIC + pictureId,
          fit: BoxFit.fill,
          errorBuilder: (context, exception, stacktrace) => Icon(
            Icons.error,
            size: MediaQuery.of(context).size.width * 1 / 6,
          ),
          width: MediaQuery.of(context).size.width * 1 / 6,
          height: MediaQuery.of(context).size.width * 1 / 6,
        ),
      ),
    );
  }

  Widget name(String restaurantName) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Text(
        restaurantName,
        style: blackTextTheme.headline5,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget ratingSection(double rating) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(3),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.star,
              color: Colors.yellow,
              size: 15,
            ),
            Text(
              rating.toString(),
              style: blackTextTheme.button,
            ),
          ],
        ),
      ),
    );
  }
}
