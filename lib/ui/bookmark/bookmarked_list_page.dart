import 'package:flutter/material.dart';
import 'package:indo_restaurant/common/navigators.dart';
import 'package:indo_restaurant/common/style.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail.dart';
import 'package:indo_restaurant/provider/restaurant_provider.dart';
import 'package:indo_restaurant/ui/detail/detail_restaurant_page.dart';
import 'package:indo_restaurant/utils/status.dart';
import 'package:provider/provider.dart';

class BookmarkedListPage extends StatefulWidget {
  static const routeName = '/bookmark_list_page';

  @override
  _BookmarkedListPageState createState() =>
      _BookmarkedListPageState();
}

class _BookmarkedListPageState extends State<BookmarkedListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Bookmarked Restaurant"), actions: <Widget>[
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                Provider.of<RestaurantProvider>(context, listen: false)
                    .deleteBookmarkedList();
              },
              child: Icon(Icons.clear),
            )),
      ]),
      body: Consumer<RestaurantProvider>(
        builder: (context, state, _) {
          if (state.stateBookmark == Status.Loading) {
            return Center(child: CircularProgressIndicator());
          } else if (state.stateBookmark == Status.HasData) {
            return listViewAdapter(state.bookmarkedList);
          } else if (state.stateBookmark == Status.NoData) {
            return Center(child: Text(state.message));
          } else if (state.stateBookmark == Status.Error) {
            return Center(
              child: Text('gagal memuat data : ' + state.message),
            );
          } else {
            return Center(child: Text('gagal memuat data'));
          }
        },
      ),
    );
  }

  Widget listViewAdapter(List<RestaurantDetail> restaurants) {
    return ListView.builder(
      padding: EdgeInsets.only(top: 10),
      shrinkWrap: true,
      itemCount: restaurants.length,
      itemBuilder: (context, index) {
        return Consumer<RestaurantProvider>(builder: (context, state, child) {
          return GestureDetector(
            child: Container(
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 5, bottom: 5, top: 5),
                        child: Stack(
                          children: [
                            Container(
                              height: MediaQuery.of(context).size.width * 1 / 6 + 12,
                              color: Colors.white,
                            ),
                            restaurantPicture(restaurants[index]),
                            Positioned(
                                bottom: 0,
                                left: 2,
                                right: 2,
                                child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x54000000),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                        ),
                                      ],
                                    ),
                                    child: restaurantRating(restaurants[index]))),
                          ],
                        )),
                    Container(
                      width: MediaQuery.of(context).size.width * 5 / 6 - 25,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          restaurantName(restaurants[index]),
                          restaurantCity(restaurants[index]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              Navigators.navigateData(DetailRestaurantPage.routeName,
                  DetailRestaurantArgs(restaurants[index].id, restaurants[index].name));
            },
          );
        });
      },
    );
  }

  Widget restaurantPicture(RestaurantDetail restaurant) => ClipRRect(
    borderRadius: BorderRadius.circular(10),
    child: Image.network(
      ApiService.BASE_PIC + restaurant.pictureId,
      fit: BoxFit.fill,
      errorBuilder: (context, exception, stacktrace) => Icon(
        Icons.error,
        size: MediaQuery.of(context).size.width * 1 / 6,
      ),
      width: MediaQuery.of(context).size.width * 1 / 6,
      height: MediaQuery.of(context).size.width * 1 / 6,
    ),
  );

  Widget restaurantName(RestaurantDetail restaurant) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Text(
        restaurant.name,
        style: blackTextTheme.headline5,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget restaurantCity(RestaurantDetail restaurant) => Container(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.location_on,
          size: 20,
          color: secondary,
        ),
        Text(
          restaurant.city,
          style: blackTextTheme.subtitle1,
        ),
      ],
    ),
  );

  Widget restaurantRating(RestaurantDetail restaurant) => ClipRRect(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    child: Container(
      color: Colors.white,
      padding: EdgeInsets.all(3),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.star,
            color: Colors.yellow,
            size: 15,
          ),
          Text(
            restaurant.rating.toString(),
            style: blackTextTheme.button,
          ),
        ],
      ),
    ),
  );
}
