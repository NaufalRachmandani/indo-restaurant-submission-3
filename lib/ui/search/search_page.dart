import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:indo_restaurant/common/navigators.dart';
import 'package:indo_restaurant/common/style.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/detail_restaurant_args.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant.dart';
import 'package:indo_restaurant/provider/restaurant_provider.dart';
import 'package:indo_restaurant/ui/detail/detail_restaurant_page.dart';
import 'package:indo_restaurant/utils/status.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  static const routeName = '/search_page';

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _searchController = TextEditingController();
  String _search = '';

  @override
  void initState() {
    _searchController.addListener(
      () {
        setState(() {
          _search = _searchController.text;
        });
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Restaurant"),
      ),
      body: Column(
        children: [
          searchFieldSection(),
          Expanded(child: listSection()),
        ],
      ),
    );
  }

  Widget searchFieldSection() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(color: primary, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: TextField(
        controller: _searchController,
        autofocus: false,
        keyboardType: TextInputType.text,
        style: blackTextTheme.subtitle1,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          hintText: "Mau cari restaurant apa?",
          suffixIcon: _search.isEmpty
              ? null
              : IconButton(
                  onPressed: () {
                    _searchController.clear();
                    Provider.of<RestaurantProvider>(context, listen: false)
                        .getSearchRestaurants("a");
                  },
                  icon: Icon(Icons.cancel, color: Colors.grey),
                ),
        ),
        onChanged: (value) {
          if (value != "") {
            Provider.of<RestaurantProvider>(context, listen: false)
                .getSearchRestaurants(value);
          }
        },
      ),
    );
  }

  Widget listSection() {
    return Consumer<RestaurantProvider>(
      builder: (context, state, _) {
        if (state.stateSearch == Status.Loading) {
          return Center(child: CircularProgressIndicator());
        } else if (state.stateSearch == Status.HasData) {
          return listViewAdapter(state.searchedRestaurantList);
        } else if (state.stateSearch == Status.NoData) {
          return Center(child: Text(state.messageSearch));
        } else if (state.stateSearch == Status.Error) {
          return Center(
            child: Text(state.messageSearch),
          );
        } else {
          return Center(child: Text('gagal memuat data'));
        }
      },
    );
  }

  Widget listViewAdapter(List<Restaurant> restaurantList) {
    return ListView.builder(
        padding: EdgeInsets.only(top: 10),
        shrinkWrap: true,
        itemCount: restaurantList.length,
        itemBuilder: (context, index) {
            var restaurant = restaurantList[index];
            return GestureDetector(
              onTap: () {
                Navigators.navigateData(DetailRestaurantPage.routeName,
                    DetailRestaurantArgs(restaurant.id, restaurant.name));
              },
              child: Container(
                child: Card(
                  color: Colors.white,
                  shape:
                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Row(
                    children: [
                      pictureAndRatingSection(restaurant.pictureId, restaurant.rating),
                      nameSection(restaurant.name),
                    ],
                  ),
                ),
              ),
            );
          });
  }

  Widget nameSection(String restaurantName) {
    return Container(
      width: MediaQuery.of(context).size.width * 5 / 6 - 25,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          name(restaurantName),
        ],
      ),
    );
  }

  Widget pictureAndRatingSection(String pictureId, double rating) {
    return Container(
        margin: EdgeInsets.only(left: 5, bottom: 5, top: 5),
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.width * 1 / 6 + 12,
              color: Colors.white,
            ),
            restaurantPicture(pictureId),
            Positioned(
                bottom: 0,
                left: 2,
                right: 2,
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: primary,
                          spreadRadius: 1,
                          blurRadius: 1,
                        ),
                      ],
                    ),
                    child: ratingSection(rating))),
          ],
        ));
  }

  Widget restaurantPicture(String pictureId) {
    return Hero(
      tag: pictureId,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.network(
          ApiService.BASE_PIC + pictureId,
          fit: BoxFit.fill,
          errorBuilder: (context, exception, stacktrace) => Icon(
            Icons.error,
            size: MediaQuery.of(context).size.width * 1 / 6,
          ),
          width: MediaQuery.of(context).size.width * 1 / 6,
          height: MediaQuery.of(context).size.width * 1 / 6,
        ),
      ),
    );
  }

  Widget name(String restaurantName) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Text(
        restaurantName,
        style: blackTextTheme.headline5,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget ratingSection(double rating) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(3),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.star,
              color: Colors.yellow,
              size: 15,
            ),
            Text(
              rating.toString(),
              style: blackTextTheme.button,
            ),
          ],
        ),
      ),
    );
  }
}
