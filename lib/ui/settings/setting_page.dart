import 'package:flutter/material.dart';
import 'package:indo_restaurant/common/style.dart';
import 'package:indo_restaurant/provider/scheduling_provider.dart';
import 'package:provider/provider.dart';

class SettingPage extends StatefulWidget {
  static const routeName = '/settings_page';

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  void initState() {
    Provider.of<SchedulingProvider>(context, listen: false).getSwitchState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        child: Center(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset(
                        'images/calendar.png',
                        height: 40,
                        width: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(
                          "Info Restaurant Harian",
                          style: blackTextTheme.bodyText2,
                        ),
                      ),
                    ],
                  ),
                  Consumer<SchedulingProvider>(
                    builder: (context, state, _) {
                      return Switch(
                          value: state.isScheduled,
                          onChanged: (bool value) {
                            state.saveSwitchState(value);
                            state.scheduledRestaurant(value);
                          });
                    },
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  "Mengaktifkan fitur ini dapat membuat anda menerima notifikasi dari sebuah restaurant setiap hari jam 11 pagi.",
                  style: blackTextTheme.subtitle1,
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
