import 'package:flutter/material.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

import 'home/restaurant_list_page.dart';

class SplashScreen extends StatelessWidget {
  static const routeName = '/splash_screen';

  @override
  Widget build(BuildContext context) {
    return SplashScreenView(
      navigateRoute: RestaurantListPage(),
      duration: 3000,
      imageSize: 200,
      imageSrc: "images/logo.png",
      backgroundColor: Colors.white,
    );
  }
}
