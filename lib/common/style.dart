import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final Color primary = Color(0xFF0381CE);
final Color primaryLight = Color(0xFFCDEAEF);
final Color secondary = Color(0xFF75BD29);
final Color black = Color(0xFF222222);

final blackTextTheme = TextTheme(
  headline1: GoogleFonts.rubik(
      fontSize: 82,
      fontWeight: FontWeight.w300,
      letterSpacing: -1.5,
      color: black),
  headline2: GoogleFonts.rubik(
      fontSize: 51,
      fontWeight: FontWeight.w300,
      letterSpacing: -0.5,
      color: black),
  headline3: GoogleFonts.rubik(
      fontSize: 41, fontWeight: FontWeight.w400, color: black),
  headline4: GoogleFonts.rubik(
      fontSize: 29,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: black),
  headline5: GoogleFonts.rubik(
      fontSize: 20, fontWeight: FontWeight.w400, color: black),
  headline6: GoogleFonts.rubik(
      fontSize: 17,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.15,
      color: black),
  subtitle1: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      color: black),
  subtitle2: GoogleFonts.rubik(
      fontSize: 12,
      fontWeight: FontWeight.w300,
      letterSpacing: 0.1,
      color: black),
  bodyText1: GoogleFonts.rubik(
      fontSize: 18,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.5,
      color: black),
  bodyText2: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: black),
  button: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      letterSpacing: 1.25,
      color: black),
  caption: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.4,
      color: black),
  overline: GoogleFonts.rubik(
      fontSize: 11,
      fontWeight: FontWeight.w400,
      letterSpacing: 1.5,
      color: black),
);

final whiteTextTheme = TextTheme(
  headline1: GoogleFonts.rubik(
      fontSize: 82,
      fontWeight: FontWeight.w300,
      letterSpacing: -1.5,
      color: Colors.white),
  headline2: GoogleFonts.rubik(
      fontSize: 51,
      fontWeight: FontWeight.w300,
      letterSpacing: -0.5,
      color: Colors.white),
  headline3: GoogleFonts.rubik(
      fontSize: 41, fontWeight: FontWeight.w400, color: Colors.white),
  headline4: GoogleFonts.rubik(
      fontSize: 29,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: Colors.white),
  headline5: GoogleFonts.rubik(
      fontSize: 20, fontWeight: FontWeight.w400, color: Colors.white),
  headline6: GoogleFonts.rubik(
      fontSize: 17,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.15,
      color: Colors.white),
  subtitle1: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      color: Colors.white),
  subtitle2: GoogleFonts.rubik(
      fontSize: 12,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.1,
      color: Colors.white),
  bodyText1: GoogleFonts.rubik(
      fontSize: 18,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.5,
      color: Colors.white),
  bodyText2: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: Colors.white),
  button: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      letterSpacing: 1.25,
      color: Colors.white),
  caption: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.4,
      color: Colors.white),
  overline: GoogleFonts.rubik(
      fontSize: 11,
      fontWeight: FontWeight.w400,
      letterSpacing: 1.5,
      color: Colors.white),
);

final secondaryTextTheme = TextTheme(
  headline1: GoogleFonts.rubik(
      fontSize: 82,
      fontWeight: FontWeight.w300,
      letterSpacing: -1.5,
      color: secondary),
  headline2: GoogleFonts.rubik(
      fontSize: 51,
      fontWeight: FontWeight.w300,
      letterSpacing: -0.5,
      color: secondary),
  headline3: GoogleFonts.rubik(
      fontSize: 41, fontWeight: FontWeight.w400, color: secondary),
  headline4: GoogleFonts.rubik(
      fontSize: 29,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: secondary),
  headline5: GoogleFonts.rubik(
      fontSize: 20, fontWeight: FontWeight.w400, color: secondary),
  headline6: GoogleFonts.rubik(
      fontSize: 17,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.15,
      color: secondary),
  subtitle1: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.15,
      color: secondary),
  subtitle2: GoogleFonts.rubik(
      fontSize: 12,
      fontWeight: FontWeight.w500,
      letterSpacing: 0.1,
      color: secondary),
  bodyText1: GoogleFonts.rubik(
      fontSize: 18,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.5,
      color: secondary),
  bodyText2: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.25,
      color: secondary),
  button: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      letterSpacing: 1.25,
      color: secondary),
  caption: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.4,
      color: secondary),
  overline: GoogleFonts.rubik(
      fontSize: 11,
      fontWeight: FontWeight.w400,
      letterSpacing: 1.5,
      color: secondary),
);
