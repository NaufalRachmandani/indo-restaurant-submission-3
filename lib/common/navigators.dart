import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

class Navigators {
  static navigate(String routeName) {
    navKey.currentState?.pushNamed(routeName);
  }

  static navigateData(String routeName, Object argument) {
    navKey.currentState?.pushNamed(routeName, arguments: argument);
  }

  static navigateBack() => navKey.currentState?.pop();
}
