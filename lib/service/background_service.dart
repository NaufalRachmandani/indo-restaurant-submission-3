import 'dart:isolate';
import 'dart:ui';

import 'package:http/http.dart';
import 'package:indo_restaurant/data/api/api_service.dart';
import 'package:indo_restaurant/utils/notif_helper.dart';

import '../main.dart';

class BackgroundService {
  static BackgroundService? _instance;

  static SendPort? _uiSendPort;

  final ReceivePort receivePort = ReceivePort();

  BackgroundService._internal() {
    _instance = this;
  }

  factory BackgroundService() => _instance ?? BackgroundService._internal();

  void initializeIsolate() {
    IsolateNameServer.registerPortWithName(
      receivePort.sendPort,
      "isolate",
    );
  }

  static Future<void> callback() async {
    print('Alarm fired!');
    final NotifHelper _notificationHelper = NotifHelper();
    var result = await ApiService(Client()).getRestaurantList();
    await _notificationHelper.showNotif(
        flutterLocalNotificationsPlugin, result);
    _uiSendPort ??= IsolateNameServer.lookupPortByName("isolate");
    _uiSendPort?.send(null);
  }
}
