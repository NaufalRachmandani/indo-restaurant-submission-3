// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'restaurant_detail.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RestaurantDetailAdapter extends TypeAdapter<RestaurantDetail> {
  @override
  final int typeId = 0;

  @override
  RestaurantDetail read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RestaurantDetail(
      id: fields[0] as String,
      name: fields[1] as String,
      description: fields[2] as String,
      city: fields[3] as String,
      address: fields[4] as String,
      pictureId: fields[5] as String,
      categories: (fields[6] as List).cast<Category>(),
      menuList: fields[7] as MenuList,
      rating: fields[8] as double,
      customerReviewList: (fields[9] as List).cast<CustomerReviewList>(),
    )..bookmark = fields[10] as bool;
  }

  @override
  void write(BinaryWriter writer, RestaurantDetail obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.city)
      ..writeByte(4)
      ..write(obj.address)
      ..writeByte(5)
      ..write(obj.pictureId)
      ..writeByte(6)
      ..write(obj.categories)
      ..writeByte(7)
      ..write(obj.menuList)
      ..writeByte(8)
      ..write(obj.rating)
      ..writeByte(9)
      ..write(obj.customerReviewList)
      ..writeByte(10)
      ..write(obj.bookmark);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RestaurantDetailAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
