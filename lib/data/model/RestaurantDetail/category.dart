import 'package:hive/hive.dart';
part 'category.g.dart';

@HiveType(typeId: 1)
class Category {
  Category({
    required this.name,
  });

  @HiveField(0)
  String name;

  Map<String, dynamic> toJson() => {
        "name": name,
      };

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        name: json["name"],
      );
}
