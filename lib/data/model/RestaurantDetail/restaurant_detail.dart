import 'package:hive/hive.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/menu_list.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/customer_review_list.dart';

import 'category.dart';
part 'restaurant_detail.g.dart';

@HiveType(typeId: 0)
class RestaurantDetail extends HiveObject{
  RestaurantDetail({
    required this.id,
    required this.name,
    required this.description,
    required this.city,
    required this.address,
    required this.pictureId,
    required this.categories,
    required this.menuList,
    required this.rating,
    required this.customerReviewList,
  });

  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String description;
  @HiveField(3)
  String city;
  @HiveField(4)
  String address;
  @HiveField(5)
  String pictureId;
  @HiveField(6)
  List<Category> categories;
  @HiveField(7)
  MenuList menuList;
  @HiveField(8)
  double rating;
  @HiveField(9)
  List<CustomerReviewList> customerReviewList;
  @HiveField(10)
  bool bookmark = false;

  factory RestaurantDetail.fromJson(Map<String, dynamic> json) =>
      RestaurantDetail(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        city: json["city"],
        address: json["address"],
        pictureId: json["pictureId"],
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))),
        menuList: MenuList.fromJson(json["menus"]),
        rating: json["rating"].toDouble(),
        customerReviewList: List<CustomerReviewList>.from(
            json["customerReviews"].map((x) => CustomerReviewList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "city": city,
        "address": address,
        "pictureId": pictureId,
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "menus": menuList.toJson(),
        "rating": rating,
        "customerReviews":
            List<dynamic>.from(customerReviewList.map((x) => x.toJson())),
      };
}
