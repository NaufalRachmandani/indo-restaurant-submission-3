class DetailRestaurantArgs {
  final String restaurantId;
  final String restaurantName;

  DetailRestaurantArgs(this.restaurantId, this.restaurantName);
}
