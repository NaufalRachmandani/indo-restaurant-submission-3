// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_list.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuListAdapter extends TypeAdapter<MenuList> {
  @override
  final int typeId = 3;

  @override
  MenuList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuList(
      foodList: (fields[0] as List).cast<Category>(),
      drinkList: (fields[1] as List).cast<Category>(),
    );
  }

  @override
  void write(BinaryWriter writer, MenuList obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.foodList)
      ..writeByte(1)
      ..write(obj.drinkList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
