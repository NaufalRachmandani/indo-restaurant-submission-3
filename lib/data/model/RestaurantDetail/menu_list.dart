import 'package:hive/hive.dart';

import 'category.dart';
part 'menu_list.g.dart';

@HiveType(typeId: 3)
class MenuList {
  MenuList({
    required this.foodList,
    required this.drinkList,
  });

  @HiveField(0)
  List<Category> foodList;
  @HiveField(1)
  List<Category> drinkList;

  Map<String, dynamic> toJson() => {
        "foods": List<dynamic>.from(foodList.map((x) => x.toJson())),
        "drinks": List<dynamic>.from(drinkList.map((x) => x.toJson())),
      };

  factory MenuList.fromJson(Map<String, dynamic> json) => MenuList(
        foodList:
            List<Category>.from(json["foods"].map((x) => Category.fromJson(x))),
        drinkList: List<Category>.from(
            json["drinks"].map((x) => Category.fromJson(x))),
      );
}
