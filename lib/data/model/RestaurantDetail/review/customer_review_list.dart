import 'package:hive/hive.dart';
part 'customer_review_list.g.dart';

@HiveType(typeId: 2)
class CustomerReviewList {
  CustomerReviewList({
    required this.name,
    required this.review,
    required this.date,
  });

  @HiveField(0)
  String name;
  @HiveField(1)
  String review;
  @HiveField(2)
  String date;

  Map<String, dynamic> toJson() => {
        "name": name,
        "review": review,
        "date": date,
      };

  factory CustomerReviewList.fromJson(Map<String, dynamic> json) =>
      CustomerReviewList(
        name: json["name"],
        review: json["review"],
        date: json["date"],
      );
}
