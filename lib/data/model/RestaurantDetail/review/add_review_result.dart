import 'customer_review_list.dart';

class AddReviewResult {
  AddReviewResult({
    required this.error,
    required this.message,
    required this.customerReviewList,
  });

  bool error;
  String message;
  List<CustomerReviewList> customerReviewList;

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "customerReviews":
            List<dynamic>.from(customerReviewList.map((x) => x.toJson())),
      };

  factory AddReviewResult.fromJson(Map<String, dynamic> json) =>
      AddReviewResult(
        error: json["error"],
        message: json["message"],
        customerReviewList: List<CustomerReviewList>.from(
            json["customerReviews"].map((x) => CustomerReviewList.fromJson(x))),
      );
}
