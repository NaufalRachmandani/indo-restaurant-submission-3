// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_review_list.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CustomerReviewListAdapter extends TypeAdapter<CustomerReviewList> {
  @override
  final int typeId = 2;

  @override
  CustomerReviewList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CustomerReviewList(
      name: fields[0] as String,
      review: fields[1] as String,
      date: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CustomerReviewList obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.review)
      ..writeByte(2)
      ..write(obj.date);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CustomerReviewListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
