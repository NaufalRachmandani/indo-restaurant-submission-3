import 'restaurant_detail.dart';

class RestaurantDetailResult {
  RestaurantDetailResult({
    required this.error,
    required this.message,
    required this.restaurantDetail,
  });

  bool error;
  String message;
  RestaurantDetail restaurantDetail;

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "restaurant": restaurantDetail.toJson(),
      };

  factory RestaurantDetailResult.fromJson(Map<String, dynamic> json) =>
      RestaurantDetailResult(
        error: json["error"],
        message: json["message"],
        restaurantDetail: RestaurantDetail.fromJson(json["restaurant"]),
      );
}
