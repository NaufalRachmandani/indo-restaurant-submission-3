import 'package:indo_restaurant/data/model/RestaurantList/restaurant.dart';

class RestaurantListResult {
  RestaurantListResult({
    required this.error,
    required this.message,
    required this.count,
    required this.restaurantList,
  });

  bool error;
  String message;
  int count;
  List<Restaurant> restaurantList;

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "count": count,
        "restaurants":
            List<dynamic>.from(restaurantList.map((x) => x.toJson())),
      };

  factory RestaurantListResult.fromJson(Map<String, dynamic> json) =>
      RestaurantListResult(
        error: json["error"],
        message: json["message"],
        count: json["count"],
        restaurantList: List<Restaurant>.from(
            json["restaurants"].map((x) => Restaurant.fromJson(x))),
      );
}
