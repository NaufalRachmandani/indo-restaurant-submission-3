import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:indo_restaurant/data/model/RestaurantDetail/restaurant_detail_result.dart';
import 'package:indo_restaurant/data/model/RestaurantDetail/review/add_review_result.dart';
import 'package:indo_restaurant/data/model/RestaurantList/restaurant_result.dart';
import 'package:indo_restaurant/data/model/RestaurantSearch/search_restaurant_result.dart';

class ApiService {
  final Client client;

  ApiService(this.client);

  static final String BASE_URL = 'https://restaurant-api.dicoding.dev/';
  static final String BASE_PIC =
      'https://restaurant-api.dicoding.dev/images/small/';
  static final String API_KEY = '12345';

  Future<RestaurantListResult> getRestaurantList() async {
    final response = await client.get(Uri.parse(BASE_URL + "list"));
    if (response.statusCode == 200) {
      return RestaurantListResult.fromJson(json.decode(response.body));
    } else {
      throw Exception('Gagal mendapatkan data dari internet');
    }
  }

  Future<RestaurantDetailResult> getRestaurant(String id) async {
    final response = await client.get(Uri.parse(BASE_URL + "detail/" + id));
    if (response.statusCode == 200) {
      return RestaurantDetailResult.fromJson(json.decode(response.body));
    } else {
      throw Exception('Gagal mendapatkan data dari internet');
    }
  }

  Future<SearchRestaurantResult> searchRestaurantList(String query) async {
    final response =
        await client.get(Uri.parse(BASE_URL + "search?q=" + query));
    print("searchRestaurants with $query ${response.statusCode}");
    if (response.statusCode == 200) {
      return SearchRestaurantResult.fromJson(json.decode(response.body));
    } else {
      throw Exception('Gagal mendapatkan data dari internet');
    }
  }

  Future<AddReviewResult> addReview(
      String id, String name, String review) async {
    final response =
        await client.post(Uri.parse(BASE_URL + "review"), headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Auth-Token': '$API_KEY',
      'Access-Control_Allow_Origin': '*'
    }, body: {
      'id': '$id',
      'name': '$name',
      'review': '$review'
    });
    print("addReview with $id $name $review ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      return AddReviewResult.fromJson(json.decode(response.body));
    } else {
      throw Exception('Gagal menambahkan review');
    }
  }
}
